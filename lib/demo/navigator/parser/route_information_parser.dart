import 'package:flutter/material.dart';

import '../book_entity.dart';

/// 页面控制
class BookRouteInformationParser extends RouteInformationParser<BookRoutePath> {
  @override
  Future<BookRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);

    if (uri.pathSegments.isNotEmpty && uri.pathSegments.first == 'settings') {
      return BooksSettingsPath();///设置界面
    } else {
      if (uri.pathSegments.length >= 2) {
        if (uri.pathSegments[0] == 'book') {///详情界面
          return BooksDetailsPath(int.tryParse(uri.pathSegments[1])!);
        }
      }
      return BooksListPath();///列表界面
    }
  }

  @override
  RouteInformation? restoreRouteInformation(BookRoutePath configuration) {
    if (configuration is BooksListPath) {///列表
      return const RouteInformation(location: '/home');
    }
    if (configuration is BooksSettingsPath) {///设置
      return const RouteInformation(location: '/settings');
    }
    if (configuration is BooksDetailsPath) {///详情
      return RouteInformation(location: '/book/${configuration.id}');
    }
    return null;
  }
}