import 'package:flutter/material.dart';
import 'package:flutter_app/demo/overlay/page/overlay_demo_page.dart';
import 'package:flutter_app/demo/overlay/route/application.dart';
import 'package:flutter_app/demo/overlay/route/my_navigator_observer.dart';

/// 底部固定悬浮BottomNavigationBar：push也会悬浮在底部 类似tabbar
class OverlayDemo extends StatelessWidget {

  OverlayDemo({Key? key}): super(key: key) {
    Application.navigatorObserver = MyNavigatorObserver();
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const OverlayDemoPage(),
      navigatorObservers: <NavigatorObserver>[
        Application.navigatorObserver
      ],
    );
  }
}