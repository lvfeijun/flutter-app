import 'package:dio/dio.dart';
// import 'package:disk/Home/HomeFilterPop.dart';
// import 'package:disk/Model/item_model.dart';
// import 'package:disk/Model/userinfo_model.dart';
// import 'package:disk/Public/DConstant.dart';
// import 'package:disk/Public/DRPop.dart';
import 'http_url.dart';
import 'http_base.dart';


class HttpBaseReq
{

  // static postJson(String url,{Map json,idBolok comp, idBolok error}) async {
  //   Response response = await post(url, parameters: json);
  //   __pars(response, comp:comp,error: error);
  // }
  //
  // static getReq(String url,{Map json,idBolok comp,idBolok error}) async {
  //   print("\n\n\nget请求\nurl-->${url}\   json:-->${json}\n\n\n");
  //   Response response = await get(url, queryParameters: json);
  //   __pars(response, comp:comp,error: error);
  // }
  //
  // //统一解析
  // static __pars(Response response, {idBolok comp,idBolok error}) {
  //   print("\n\n\n请求返回\nurl-->${response.request.path}\ndata:-->${response.data}\n\n\n");
  //   if (response.statusCode == 200) {
  //     Map data = response.data;
  //     if (data['code'] == 10200) {
  //       comp !=null ? comp(data.containsKey("data")?data['data']:"成功") : null;
  //     } else {
  //       // AlertPop.tip("请求错误 ${response.data["message"]}");
  //       error != null ? error(response.data) : null;
  //     }
  //   }
  //   else{
  //     // AlertPop.tip("请求失败 ${response}");
  //     error != null ? error("请求失败 ${response}") : null;
  //   }
  // }

}

class HttpReq{

  //用户信息
  // static userInfo(idBolok comp) {
  //   HttpBaseReq.getReq(API_info_url,comp: (e){
  //     comp(UserInfo.fromJson(e as Map));
  //   });
  // }

  //文件列表
  // static fileList(idBolok comp,{int parentId,String keyword,FilterState fstate,idBolok error})
  // {
  //   DateTime now = DateTime.now();
  //   DateTime befday = now.add(Duration(days: -90));
  //   String createTimeStart = "${befday.year}-${befday.month}-${befday.day}";
  //   Map<String, dynamic> parameters ={
  //     'createTimeStart' : createTimeStart,//时间 当前-3个月
  //     'pageNum' : '1',
  //     'parentId' : parentId??"",
  //     'pageSize' : '20',
  //     'keyword' :keyword??""
  //   };
  //   if (fstate != null){
  //     parameters.addAll(fstate.filterjson);
  //   }
  //
  //   HttpBaseReq.postJson(API_list_url, json: parameters,comp: (e){
  //     List items = e;
  //     comp(items.map<Item>((item) {
  //       return Item.fromJson(item);
  //     }).toList());
  //   });
  // }

  //文件夹列表
  // static folderList(int parentId,idBolok comp,{idBolok error})
  // {
  //   fileList((e) {
  //     List<Item> list = e;
  //     List<Item> fobjs = [];
  //     list.forEach((element) {
  //       if (element.type == 0){
  //         fobjs.add(element);
  //       }
  //     });
  //     comp(fobjs);
  //   },parentId: parentId,error: error);
  // }

  //文件删除
  // static filedeletes(String ids,idBolok comp)
  // {
  //   HttpBaseReq.postJson(API_delete_url, json: {'ids' : ids},comp: comp);
  // }

  /*
  文件/夹改名
  type  true 文件夹、false文件
  * */
  // static filerename(Item model,  String rname,idBolok comp)
  // {
  //   String url = model.isfolder?API_updateDirectoryname_url:API_updateFilename_url;
  //   HttpBaseReq.postJson(url, json: {'name': rname, 'id': model.id,},comp: (_){
  //     model.name = rname;
  //     comp(true);
  //   });
  // }

  //创建文件夹、
  // static floderNew(String name,int parentId,idBolok comp)
  // {
  //   HttpBaseReq.postJson(API_create_url, json: {'name': name, 'parentId': parentId},comp: (e){
  //     comp(Item.fromJson(e));
  //   });
  // }

  //移动文件夹、
  // static fileMove(List idList,int targetDirId, idBolok comp)
  // {
  //   HttpBaseReq.postJson(API_move_url, json: {'targetDirId': targetDirId,"idList":idList},comp: (_){
  //     comp(1);
  //     Toast.success("移动成功");
  //   });
  // }

  //复制文件夹、
  // static fileCopy(List idList,int targetDirId, idBolok comp)
  // {
  //   HttpBaseReq.postJson(API_copy_url, json: {'targetDirId': targetDirId,"idList":idList},comp: (_){
  //     comp(1);
  //     Toast.success("复制成功");
  //   });
  // }

  //搜索文件
  // static fileSearch(String keyword,int parentId, idBolok comp)
  // {
  //   String url = parentId==0?API_list_url:API_search_url;
  //   HttpBaseReq.postJson(url, json: {'keyword': keyword,"parentId":parentId==0?"":parentId},comp: (e){
  //     List items = e;
  //     comp(items.map<Item>((item) {
  //       return Item.fromJson(item);
  //     }).toList());
  //   });
  // }

}


