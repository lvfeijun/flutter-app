import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
// import 'package:disk/Home/HomeFilterPop.dart';
// import 'package:encrypt/encrypt.dart';
import 'package:flutter_cipher/flutter_cipher.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'http_url.dart';
import 'http_base.dart';
// import '../Model/userinfo_model.dart';


// 最近文件列表
// Future<Response> postNearList({FilterState fstate}) async {
//   DateTime now = DateTime.now();
//   DateTime befday = now.add(Duration(days: -90));
//   String createTimeStart = "${befday.year}-${befday.month}-${befday.day}";
//   Map<String, dynamic> parameters ={
//     'createTimeStart' : createTimeStart,//时间 当前-3个月
//     'pageNum' : '1',
//     'parentId' : '',
//     'pageSize' : '20',
//     'keyword' :''
//   };
//   if (fstate != null){
//     parameters.addAll(fstate.filterjson);
//   }
//   return postList(parameters);
// }
// 文件夹文件列表
Future<Response> postDirectoryList(int parentId) async {
  Map<String, dynamic> parameters ={
    'sortVo' : {
      'sortDirect' : 'ASC',
      'sortField' : 'updateTime'
    },
    'pageNum' : '1',
    'parentId' : parentId,
    'pageSize' : '20',
    'keyword' :''
  };
  return postList(parameters);
}
//文件列表
Future<Response> postList(Map<String, dynamic> parameters) async {
  return post(API_list_url, parameters: parameters);
}
//创建文件夹
Future<Response> postCreate(String name,int parentId) async {
  Map<String, dynamic> parameters ={
    'name' : name,
    'parentId' : parentId,
  };
  return post(API_create_url,parameters: parameters);
}
//改名
Future<Response> updatename(bool type, String name, int id) async {
  Map<String, dynamic> parameters = {
    'name': name,
    'id': id,
  };
  return post(type?API_updateDirectoryname_url:API_updateFilename_url
      , parameters: parameters,data: parameters);
}
//删除文件/夹
Future<Response> postDelObjs(String ids) async {
  Map<String, dynamic> parameters ={
    'ids' : ids,
  };
  return post(API_delete_url,parameters: parameters);
}



Future<Response> postListv2(
 String keyword, int parentId
    ) async {

  Map<String, dynamic> parameters ={
    'parentId' : parentId,
     'keyword' : keyword
  };
  return post(
      API_search_url,
      parameters: parameters,data: parameters);
}

//获取个人信息
Future<Response> getInfo() async {
  return get(API_info_url);
}

//
Future<Response> getCreate() async {
  return get(
      'http://zttest.bndxqc.com/clouddisk/storage/account/create');
}

//模拟登陆获取token
Future<void> login() async {
  String access_token = HttpManager().access_token;
  if(access_token !=null && access_token.length > 0){
    print('have access_token: ${access_token}');
    return ;
  }
  print('request access_token: ${access_token}');

  // md5 加密
  String generate_MD5(String data) {
    var content = new Utf8Encoder().convert(data);
    var digest = md5.convert(content);
    // 这里其实就是 digest.toString()
    return hex.encode(digest.bytes);
  }

  String psd = passwork+'xqc1254548787244';
  String md5s = generate_MD5(psd);

  Future<String> encodeString(String content) async{
    var publicKeyStr = await rootBundle.loadString(Api_rsa);
    //需要替换不维护的库
    var publicKey = RSAKeyParser().parse(publicKeyStr);
    Asymmetric rsa = Cipher.getAsymmetricInstance(RSA(publicKey: publicKey));

    int inputLen = content.length;
    int maxLen = 117;
    List<int> totalBytes = [];
    for (var i = 0; i < inputLen; i += maxLen) {
      int endLen = inputLen - i;
      String item;
      if (endLen > maxLen) {
        item = content.substring(i, i + maxLen);
      }
      else {
        item = content.substring(i, i + endLen);
      }
      totalBytes.addAll(rsa.encryptPublic(item).bytes);
    }
    return base64.encode(totalBytes);
  }

  // print('md5:$md5s');
  String encode = await encodeString(md5s);
  // print("rsa1:$encode");

  Map<String, dynamic> data = {
    "username":name,
    "channel":"mobile",
    "password":encode
  };
  // return await post(url,data: data);

  // Future<UserInfo> getInfo1() async {
  //   final response = await getInfo();
  //   if (response.statusCode == 200) {
  //     print('getInfo : ${response.data['data']}');
  //     return UserInfo.fromJson(response.data['data']);
  //   } else {
  //     throw Exception('statusCode:${response.statusCode}');
  //   }
  // }

  final response = await post(API_login_url,data: data);
  print('response');
  print(response);
  if (response.statusCode == 200) {
    print('response.data');
    print(response.data);
    HttpManager().access_token = response.data['data']['access_token'];
    // print('access_token: ${HttpManager().access_token}');

    // getInfo1().then((UserInfo info){
    //   UserInfoMamager().userinfo = info;
    //   print('getInfo : ${info}');
    //
    // }).catchError((e) {
    //   print('-----错误$e');
    // }).whenComplete(() {
    //   print('完毕!');
    // });
  } else {
    throw Exception('statusCode:${response.statusCode}');
  }
}
