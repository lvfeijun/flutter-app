//接口文档
//http://confluence.bndxqc.com/pages/viewpage.action?pageId=54730727

String Api_DC_hosts = "http://zttest.bndxqc.com/";

String Api_rsa = "res/rsa_public_key_test.pem";

String API_login_url = Api_DC_hosts+'im-control/notoken/userClientLoginV2';//登录
String API_list_url = Api_DC_hosts+'clouddisk/storage/directory/list';//文件列表
String API_create_url = Api_DC_hosts+'clouddisk/storage/directory/create';//创建文件夹
String API_info_url = Api_DC_hosts+'clouddisk/storage/account/info';//用户信息
String API_search_url = Api_DC_hosts+'clouddisk/storage/directory/list/v2';//用户信息
String API_updateDirectoryname_url = Api_DC_hosts+'clouddisk/storage/directory/updatePersonalDirectoryname';//修改文件夹名
String API_updateFilename_url = Api_DC_hosts+'clouddisk/storage/directory/updateFilename';//修改文件名
String API_delete_url = Api_DC_hosts+'clouddisk/storage/directory/delete';//删除文件夹或者文件
String API_move_url = Api_DC_hosts+'clouddisk/storage/directory/move';//移动文件或者文件夹
String API_copy_url = Api_DC_hosts+'clouddisk/storage/directory/copy';//复制文件或者文件夹

//更新hosts
API_updateHost(int i) {
  switch (i) {
    case 0:
      Api_DC_hosts = "http://192.168.14.152:8080/";
      Api_rsa = "res/rsa_public_key_d.pem";
      break;
    case 1:
      Api_DC_hosts = "http://zttest.bndxqc.com/";
      Api_rsa = "res/rsa_public_key_test.pem";
      break;
    case 2:
      Api_DC_hosts = "https://ztty.bndxqc.com/";
      Api_rsa = "res/rsa_public_key_tr.pem";
      break;
    case 3:
      Api_DC_hosts = "https://zt.bndxqc.com/";
      Api_rsa = "res/rsa_public_key_r.pem";
      break;
  }
  print("API_Hose: ${Api_DC_hosts}");
}
