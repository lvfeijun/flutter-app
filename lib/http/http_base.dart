import 'package:dio/dio.dart';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'http_manager.dart';

int receiveTimeout = 60;
int sendTimeout = 60;
// String passwork = 'a123456';
// String name = '18664690001';//18029236835
String passwork = '123456';
String name = '18818707459';//18029236835

class HttpManager {

  String access_token = '';

  // 静态变量_instance，存储唯一对象
  static HttpManager? _instance;
  // instance的getter方法，HttpManager.instance获取对象
  static HttpManager get instance => _getInstance();

  // 获取对象
  static HttpManager _getInstance() {
    if (_instance == null) {
      print('new');
      // 使用私有的构造方法来创建对象
      _instance = HttpManager._getInstance();
    }
    return _instance!;
  }

  static setproxy(){
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.findProxy = (uri) {
        //proxy all request to localhost:8888
        return "PROXY 10.2.106.147:61681";
      };
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
    };
    print('set proxy');
  }

  static Dio dio = new Dio();

  static Future <Response> post(String url,
      {Map<String,dynamic>? data,
        Map<String,dynamic>? parameters,
        Map<String,dynamic>? head}) {

    /// 自定义Header
    // Map<String, dynamic> httpHeaders = {
    //   'Accept': 'application/json,*/*',
    //   'Content-Type': 'multipart/form-data',
    //   // "Authorization": headers
    // };
    String access_token = HttpManager().access_token;
    if(access_token != null){
      dio.options.headers['Authorization'] = 'Bearer '+access_token;
      print('add Authorization : ${dio.options.headers['Authorization']}');
      setproxy();
    }




    print('post请求:');
    print('url: '+url);
    print('data:');
    print(data);
    print('parameters:');
    print(parameters);
    FormData? formData = null;
    if( data != null){
      formData = FormData.fromMap(data!);
    }
    //1.创建配置,什么方式请求
    if (head != null){
      head.forEach((key, value) {
        // ops[key] = value;
        dio.options.headers[key] = '${value}';

        print("--->key:${key}  value:${value}");

      });
    }

    print("--->head:${dio.options.headers}");
    // Options options = Options(receiveTimeout:receiveTimeout,headers:head as Map<String,dynamic>);

    return dio.post(url,data: formData??(parameters??{}));
  }
  static Future <Response> request(String url,
      {String? method,
        String? headers,
        Map? queryParameters,
        int? timeOut}) {

    /// 自定义Header
    Map<String,dynamic> httpHeaders = method=='get' ? {
      "Authorization": 'Bearer '+HttpManager.instance.access_token
    } : {
      'Accept': 'application/json,*/*',
      'Content-Type': 'multipart/form-data',
      "Authorization": 'Bearer '+HttpManager.instance.access_token
    };
    FormData formData = FormData.fromMap(queryParameters as Map<String,dynamic>);

    //1.创建配置,什么方式请求
    final options = Options(method: method, receiveTimeout:receiveTimeout,headers:httpHeaders,contentType:'multipart/form-data');
    // 2.发网络请求
    return dio.request(
      url,
      data: formData,
      queryParameters: queryParameters as Map<String,dynamic>,
      options: options,
    );
  }

}

Future<Response> get(String url,
    {String? headers,
      Map<String,dynamic>? queryParameters,
      int? timeOut}) {
  return HttpManager.request(url,
      queryParameters: queryParameters,
      headers:headers, method: 'get', timeOut: timeOut);
}


Future<Response> post(String url,
    {Map<String,dynamic>? data,
      Map<String,dynamic>? parameters}) {
  return HttpManager.post(url, data: data,parameters:parameters);
}

