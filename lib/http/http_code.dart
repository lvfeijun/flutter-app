///网络请求Method
class http_code {

  /// 成功
  static const PLHttpRequestStateSucceed = 10200;

  /// 您已经在其他设备登录
  static const PLHttpRequestStateOtherDeviceLogin = 10002;

  /// 失败
  static const PLHttpRequestStateFail = 10500;

  /// 用户名不能为空/密码不能为空/参数错误
  static const PLHttpRequestStateParamError = 10005;

  /// 请求失败
  static const PLHttpRequestStateServerException = 10006;

  /// 密码错误
  static const PLHttpRequestStatePasswordError = 10007;

  /// token 无效 - 强制刷新用户token
  static const PLHttpRequestStateTokenInvalid = 10008;

  /// 不支持刷新token
  static const PLHttpRequestStateNoRefreshToken = 10009;

  /// 刷新token无效
  static const PLHttpRequestStateRefreshTokenInvalid = 10011;

  ///刷新token 次数达到上限
  static const PLHttpRequestStateRefreshTokenFull = 10012;

  ///用户未注册
  static const PLHttpRequestStateUserNotRegistered = 11001;

  ///请求超时
  static const PLHttpRequestStateTimeOut = 11013;

  ///用户未关联到平台(没有推送id过来)
  static const PLHttpRequestStateUserNotJoin = 12016;

  ///rsa加解密相关错误
  static const PLHttpRequestStateRSAError = 11012;

  ///员工未同步
  static const PLHttpRequestStateStaffNoUpdate = 12031;

  ///账号未同步
  static const PLHttpRequestStateUserNoUpdate = 12011;

  ///企业未开通
  static const PLHttpRequestStateCompanyUnOpen = 12021;
}