import 'package:flutter_app/generated/json/base/json_convert_content.dart';

class GoodsSortEntity with JsonConvert<GoodsSortEntity> {
  late String id;
  late String name;
}
