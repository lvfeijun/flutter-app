///选择城市
import 'package:azlistview/azlistview.dart';
import 'package:flutter_app/generated/json/base/json_convert_content.dart';

///城市模型
class CityEntity with JsonConvert<CityEntity>, ISuspensionBean {
  late String name;
  late String cityCode;
  late String firstCharacter;

  @override
  String getSuspensionTag() {
    return firstCharacter;
  }
}
