import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sp_util/sp_util.dart';
import 'package:flutter_app/res/constant.dart';
import 'package:flutter_gen/gen_l10n/deer_localizations.dart';
import 'package:flutter_app/login/widgets/my_text_field.dart';
import 'package:flutter_app/res/resources.dart';
import 'package:flutter_app/routers/fluro_navigator.dart';
import 'package:flutter_app/store/store_router.dart';
import 'package:flutter_app/util/change_notifier_manage.dart';
import 'package:flutter_app/util/other_utils.dart';
import 'package:flutter_app/widgets/my_app_bar.dart';
import 'package:flutter_app/widgets/my_button.dart';
import 'package:flutter_app/widgets/my_scroll_view.dart';

import '../login_router.dart';

/// design/1注册登录/index.html
class LoginPage extends StatefulWidget {

  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with ChangeNotifierMixin<LoginPage> {
  //定义一个 TextEditingController 和FocusNode
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  bool _clickable = false;

  @override
  Map<ChangeNotifier?, List<VoidCallback>?>? changeNotifier() {
    final List<VoidCallback> callbacks = <VoidCallback>[_verify];
    return <ChangeNotifier, List<VoidCallback>?>{//监听textField变化
     _nameController:callbacks,//给这两个控制器的变化 监听回调
      _passwordController:callbacks,
     _nodeText1 : null,//null 没有回调 不监听
     // _nodeText2 : null,//去掉也行
    };
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      /// 显示状态栏和导航栏
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top,SystemUiOverlay.bottom]);
    });
    _nameController.text = SpUtil.getString(Constant.phone).nullSafe;
  }

  void _verify(){///账号密码判断
    final String name = _nameController.text;
    final String password = _passwordController.text;
    bool clicoable = true;
    if(name.isEmpty || name.length < 11){
      clicoable = false;
    }
    if(password.isEmpty || password.length < 6){
      clicoable = false;
    }

    /// 状态不一样再刷新，避免不必要的setState
    if(clicoable != _clickable){
      setState((){
        _clickable = clicoable;
      });
    }
  }

  void _login(){
    SpUtil.putString(Constant.phone, _nameController.text); ///存储手机号
    NavigatorUtils.push(context, StoreRouter.auditPage);//Toast.show('登录');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        isBack: false,
        actionName: DeerLocalizations.of(context)!.verificationCodeLogin,
        onPressed: () {
          NavigatorUtils.push(context, LoginRouter.smsLoginPage);
        },
      ),
      body: MyScrollView(
        keyboardConfig: Utils.getKeyboardActionsConfig(context, <FocusNode>[_nodeText1, _nodeText2]),
        padding: EdgeInsets.only(left: 16,right: 16,top: 20),
        children: _buildBody,
      ),
    );
  }

  List <Widget> get _buildBody =>  <Widget>[
    Text(
      DeerLocalizations.of(context)!.passwordLogin,
      // style: TextStyle.t,
    ),
    Gaps.vGap16,
    MyTextField(
      key: const Key('phone'),
        focusNode: _nodeText1,
        controller: _nameController,
      maxLength: 11,
      keyboardType: TextInputType.phone,
      hintText: DeerLocalizations.of(context)!.inputUsernameHint,
    ),
    Gaps.vGap8,
    MyTextField(
      key: Key('password'),
        keyName: 'password',
        focusNode: _nodeText2,
        isInputPwd: true,
        controller: _passwordController,
      keyboardType: TextInputType.visiblePassword,
      maxLength: 16,
      hintText: DeerLocalizations.of(context)!.inputPasswordHint,
    ),
    Gaps.vGap24,
    MyButton(
      key: const Key('login'),
      onPressed: _clickable ? _login : null,
      text: DeerLocalizations.of(context)!.login,
    ),
    Container(
      height: 40,
      alignment: Alignment.centerRight,
      child: GestureDetector(
      child: Text(
        DeerLocalizations.of(context)!.forgotPasswordLink,
        key: Key('forgotPassword'),
        style: Theme.of(context).textTheme.subtitle2,
      ),
        onTap: () => NavigatorUtils.push(context, LoginRouter.resetPasswordPage),
      ),
    ),
    Gaps.vGap16,
    Container(
      alignment: Alignment.center,
      child: GestureDetector(
        child: Text(
          DeerLocalizations.of(context)!.noAccountRegisterLink,
          key: Key('noAccountRegister'),
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
        onTap: () => NavigatorUtils.push(context, LoginRouter.registerPage),
      ),
    ),
  ];
}
